use clap::Parser;
use std::path::{Path, PathBuf};

#[derive(Debug, Parser)]
#[command(author, version, about)]
pub struct Args {
    /// Source containing the photos
    source: PathBuf,
    /// Base directory where the photos should be copied to.
    target: PathBuf,
    #[arg(short, long)]
    /// Override the directory the files is copied to.
    directory: Option<String>,
    #[arg(short, long)]
    /// Only list the available photos
    list: bool,
}

impl Args {
    pub fn source(&self) -> &Path {
        &self.source
    }

    pub fn target(&self) -> &Path {
        &self.target
    }

    pub fn directory(&self) -> &Option<String> {
        &self.directory
    }

    pub fn list(&self) -> bool {
        self.list
    }
}
