use std::path::Path;

use anyhow::Result;
use zbus::{
    zvariant::{SerializeDict, Type},
    Connection, Proxy,
};

#[derive(SerializeDict, Type)]
#[zvariant(signature = "a{sv}")]
struct MountOpt {
    force: bool,
}

pub struct UDisks<'a> {
    block_device: &'a Path,
    mounted: bool,
}

impl<'a> UDisks<'a> {
    pub fn new(block_device: &'a Path) -> UDisks {
        UDisks {
            block_device,
            mounted: false,
        }
    }

    pub async fn unmount(&mut self) -> Result<()> {
        if !self.mounted {
            return Ok(());
        }

        let connection = Connection::system().await?;
        let dbus_path = format!(
            "/org/freedesktop/UDisks2/block_devices/{}",
            self.block_device.strip_prefix("/dev")?.to_str().unwrap()
        );

        let proxy = Proxy::new(
            &connection,
            "org.freedesktop.UDisks2",
            dbus_path,
            "org.freedesktop.UDisks2.Filesystem",
        )
        .await?;

        let body = MountOpt { force: true };
        proxy.call("Unmount", &body).await?;

        self.mounted = false;

        Ok(())
    }

    pub async fn mount(&mut self) -> Result<String> {
        let connection = Connection::system().await?;
        let dbus_path = format!(
            "/org/freedesktop/UDisks2/block_devices/{}",
            self.block_device.strip_prefix("/dev")?.to_str().unwrap()
        );

        let proxy = Proxy::new(
            &connection,
            "org.freedesktop.UDisks2",
            dbus_path,
            "org.freedesktop.UDisks2.Filesystem",
        )
        .await?;

        let body = MountOpt { force: false };
        let path = proxy.call("Mount", &body).await?;

        self.mounted = true;

        Ok(path)
    }
}
