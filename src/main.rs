mod args;
mod file;
mod udisk;

use std::io::{stdout, Write};

use anyhow::{anyhow, Result};
use clap::Parser;
use crossterm::queue;
use crossterm::style::{Print, PrintStyledContent, Stylize};
use file_format::FileFormat;
use walkdir::WalkDir;

use crate::udisk::UDisks;
use crate::{args::Args, file::copy};

#[tokio::main]
async fn main() -> Result<()> {
    let args = Args::parse();
    if !args.target().is_dir() {
        return Err(anyhow!("{} is not a directory", args.target().display()));
    }

    let mut stdout = stdout();
    queue!(
        stdout,
        PrintStyledContent("Source: ".green().bold()),
        Print(args.source().display()),
        Print("\n")
    )?;
    queue!(
        stdout,
        PrintStyledContent("Target: ".green().bold()),
        Print(args.target().display()),
        Print("\n")
    )?;
    stdout.flush()?;

    println!();

    let mut udisks = UDisks::new(args.source());
    let mount_path = udisks.mount().await?;
    let walk = WalkDir::new(&mount_path);

    for entry in walk {
        let entry = entry?;
        if entry.path().is_file() {
            let format = FileFormat::from_file(&entry.path())?;

            if format.media_type().starts_with("image/") {
                if !args.list() {
                    copy(&entry.path(), args.target(), args.directory())?;
                } else {
                    println!("{}", entry.file_name().to_str().unwrap());
                }
            }
        }
    }

    udisks.unmount().await?;
    Ok(())
}
