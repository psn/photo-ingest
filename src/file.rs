use std::{
    path::{Path, PathBuf},
    time::SystemTime,
};

use anyhow::{anyhow, Result};
use chrono::{Local, NaiveDateTime, TimeZone};
use crossterm::style::Stylize;

pub fn copy(file: &Path, target: &Path, override_dir: &Option<String>) -> Result<PathBuf> {
    let target_path = match override_dir {
        Some(dir) => {
            let path = target.join(dir);
            std::fs::create_dir_all(&path)?;
            path
        }
        None => create_target_path(&std::fs::metadata(file)?.created()?, target)?,
    };
    let target_file = target_path.join(file.file_name().unwrap());

    println!(
        "Copying {} into {}",
        file.file_name().unwrap().to_str().unwrap().bold(),
        target_path.display().to_string().bold()
    );

    std::fs::copy(file, &target_file)?;

    Ok(target_file)
}

fn create_target_path(created: &SystemTime, target: &Path) -> Result<PathBuf> {
    if let Some(date) = NaiveDateTime::from_timestamp_opt(
        created.duration_since(SystemTime::UNIX_EPOCH)?.as_secs() as i64,
        0,
    ) {
        let created = Local.from_utc_datetime(&date);

        let target_path = target.join(created.format("%Y/%Y-%m-%d/").to_string());
        std::fs::create_dir_all(&target_path)?;

        return Ok(target_path);
    }

    Err(anyhow!("Failed to create target path"))
}
